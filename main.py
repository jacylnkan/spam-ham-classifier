import os
import pandas as pd
from email import parser
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score


# parses email to only return the body of the message
def parse_email(full_path):
    file = open(full_path, 'rb')
    email = parser.BytesParser().parse(file)
    message = ""
    # emails may have subparts, walk() to append all subparts
    for part in email.walk():
        if part.get_content_type() == "text/plain":
            message += part.get_payload()
    return message


# iteratively yields full path to email and the full content of email
def read_email(path):
    for root, dirs, files in os.walk(path):
        for f in files:
            full_path = os.path.join(root, f)
            message = parse_email(full_path)
            yield full_path, message


# creates a dataframe containing the body of email and it's assigned class
# index is the path to the email file
def create_dataframe(path, classification):
    indexes = []
    rows = {"message": [], "class": []}

    for full_path, message in read_email(path):
        indexes.append(full_path)
        rows["message"].append(message)
        rows["class"].append(classification)

    data = pd.DataFrame(data=rows, index=indexes)
    return data


# tokenize the words inside body of message instead of preserving the actual words
# returns a sparse matrix
def vectorize_messages(data):
    vectorizer = CountVectorizer()
    doc_term_matrix = vectorizer.fit_transform(data["message"].values)
    return doc_term_matrix


# creates and returns the Naive Bayes' model using training values
def create_model(X, y):
    nb_classifier = MultinomialNB()
    nb_classifier.fit(X, y=y)
    return nb_classifier


# create dataframe of the body of all ham and spam emails with their classification
email_data = pd.concat([create_dataframe("./ham", "ham"), create_dataframe("./spam", "spam")])

# retrieve a sparse matrix count of all words in email bodies
counts = vectorize_messages(email_data)
# set the classification of emails as target
targets = email_data["class"].values

# split data into train and test sets
X_train, X_test, y_train, y_test = train_test_split(counts, targets)

# create classifier
classifier = create_model(X_train, y_train)

# calculates classification prediction of test set
predictions = classifier.predict(X_test)

# prints f1 score, assuming the "spam" classification is a positive result
print(f1_score(y_true=y_test, y_pred=predictions, pos_label="spam"))


